package com.soup.fs;

import com.soup.fs.bean.Storage;

import java.io.InputStream;

public interface FileSystem {

    /**
     * 获取文件全部信息
     * @param fileId
     * @return
     */
    Storage getStorage(String fileId);

    /**
     * 获取文件IO
     * @param fileId
     * @return
     */
    byte[] download(String fileId) throws Exception;

    /**
     * 获取文件名称
     * @param fileId
     * @return
     */
    String getName(String fileId);

    /**
     * 获取文件属性
     * @param fileId
     * @return
     */
    String getMeta(String fileId);

    /**
     * 获取文件后缀
     * @param fileId
     * @return
     */
    String getSuffix(String fileId);

    /**
     * 上传文件
     * @param inputStream
     * @param name
     * @param suffix
     * @param meta
     * @return
     */
    String upload(InputStream inputStream,String name,String suffix,String meta) throws Exception;

    /**
     * 上传文件
     * @param inputStream
     * @param originalName
     * @return
     */
    String upload(InputStream inputStream,String originalName) throws Exception;
}
