package com.soup.fs.bean;

public class Node {
    private String nodeId;
    private String path;

    public Node() {
    }

    public Node(String nodeId, String path) {
        this.nodeId = nodeId;
        this.path = path;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Node{" +
                "nodeId='" + nodeId + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
