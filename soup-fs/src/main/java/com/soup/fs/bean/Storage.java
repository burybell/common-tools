package com.soup.fs.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {
    private String fileId;
    private String name;
    private String suffix;
    private String meta;

    private List<Node> nodes = null;

    public Storage(){
        nodes = new ArrayList<Node>();
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    @Override
    public String toString() {
        return "Storage{" +
                "fileId='" + fileId + '\'' +
                ", name='" + name + '\'' +
                ", suffix='" + suffix + '\'' +
                ", meta=" + meta +
                ", nodes=" + nodes +
                '}';
    }
}
