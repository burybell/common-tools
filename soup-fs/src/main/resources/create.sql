create table node (
    nodeId char(18),
    fileId char(18),
    path varchar(255)
);

create table storage (
    fileId char(18),
    name varchar(255),
    suffix varchar(255),
    meta text
);