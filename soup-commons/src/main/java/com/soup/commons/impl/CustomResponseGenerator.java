package com.soup.commons.impl;

import com.soup.commons.ResponseGenerator;
import com.soup.commons.bean.Response;

public class CustomResponseGenerator implements ResponseGenerator {

    private final Response response = new Response();

    @Override
    public Response success(String massage) {
        return success(massage,null);
    }

    @Override
    public Response success(String massage, Object response) {
        this.response.setCode(success);
        this.response.setMassage(massage);
        this.response.setResponse(response);
        return this.response;
    }

    @Override
    public Response fail(String massage) {
        return fail(massage,null);
    }

    @Override
    public Response fail(String massage, Object response) {
        this.response.setCode(fail);
        this.response.setMassage(massage);
        this.response.setResponse(response);
        return this.response;
    }

    @Override
    public Response error(String massage) {
        return error(massage,null);
    }

    @Override
    public Response error(String massage, Object response) {
        this.response.setCode(error);
        this.response.setMassage(massage);
        this.response.setResponse(response);
        return this.response;
    }
}
