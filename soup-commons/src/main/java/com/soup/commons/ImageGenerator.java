package com.soup.commons;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageGenerator {

    /**
     * 重新设置图片的长和宽
     * @param original
     * @param width
     * @param height
     * @return
     */
    public final BufferedImage resize(BufferedImage original,int width,int height){
        if (width <= 0) {
            width = 1;
        }
        if (height <= 0) {
            height = 1;
        }
        BufferedImage newImage = new BufferedImage(width, height, original.getType());
        Graphics g = newImage.getGraphics();
        g.drawImage(original, 0, 0, width, height, null);
        g.dispose();
        return newImage;
    }

    /**
     * 按照比例放大
     * @param original
     * @param widthRatio
     * @param heightRatio
     * @return
     */
    public static BufferedImage zoomBig(BufferedImage original, Integer widthRatio, Integer heightRatio) {
        if (widthRatio <= 0) {
            widthRatio = 1;
        }
        if (heightRatio <= 0) {
            heightRatio = 1;
        }
        int width = original.getWidth() * widthRatio;
        int height = original.getHeight() * heightRatio;
        BufferedImage newImage = new BufferedImage(width, height, original.getType());
        Graphics g = newImage.getGraphics();
        g.drawImage(original, 0, 0, width, height, null);
        g.dispose();
        return newImage;
    }

    /**
     * 按照比例缩小
     * @param original
     * @param widthRatio
     * @param heightRatio
     * @return
     */
    public static BufferedImage zoomSmall(BufferedImage original, Integer widthRatio, Integer heightRatio) {
        if (widthRatio <= 0) {
            widthRatio = 1;
        }
        if (heightRatio <= 0) {
            heightRatio = 1;
        }
        int width = original.getWidth() / widthRatio;
        int height = original.getHeight() / heightRatio;
        BufferedImage newImage = new BufferedImage(width, height, original.getType());
        Graphics g = newImage.getGraphics();
        g.drawImage(original, 0, 0, width, height, null);
        g.dispose();
        return newImage;
    }

}
