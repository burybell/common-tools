package com.soup.commons;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class CheckCodeGenerator {


    BufferedImage image = null;
    //用来缓存图片（在运行内存中）
    Graphics2D gd = null;
    //画笔的2D形式
    Random random = null;
    //随机数生成的类
    StringBuffer charBuff = new StringBuffer();
    //存12345...90abc...xyzABC...XYZ，用来随机取字符
    ArrayList<String> stringBuff = new ArrayList<String>();
    //字体列表，等下在方法中随机取一个字体
    ArrayList<Color> colorBuff = new ArrayList<Color>();
    //颜色列表，等下在方法中随机取一个颜色
    StringBuffer text = new StringBuffer();
    //随机生成的文本

    //此类的构造方法，构造出画步，得到2D画笔，初始化出所有的字符颜色字体。
    public CheckCodeGenerator(){
        random = new Random();
        image = new BufferedImage(160,40,BufferedImage.TYPE_INT_RGB);
        gd = (Graphics2D)image.getGraphics();
        this.Init();
    }
    //初始化出所有的字符颜色字体
    private void Init(){
        charBuff.append("1234567890");
        charBuff.append("abcdefghigklmpqrstuvwxyz");
        charBuff.append("ABCDEFGHIGKLMPQRSTUVWXYZ");
        stringBuff.add("幼圆");
        stringBuff.add("宋体");
        stringBuff.add("华文琥珀");
        stringBuff.add("华文行楷");
        stringBuff.add("华文隶书");
        colorBuff.add(Color.RED);
        colorBuff.add(Color.BLACK);
        colorBuff.add(Color.BLUE);
        colorBuff.add(Color.GREEN);
        colorBuff.add(Color.PINK);
    }
    //获取一个随机的整数，然后在通过索引获取一个所及字符
    public char getRandomChar(){
        int rand = random.nextInt(58);
        return charBuff.charAt(rand);
    }
    //获取一个随机字体
    public String getRandomFont(){
        int rand = random.nextInt(stringBuff.size());
        return stringBuff.get(rand);
    }
    //获取一个随机颜色
    public Color getRandomColor(){
        int rand = random.nextInt(colorBuff.size());
        return colorBuff.get(rand);
    }
    //绘制随机二维码
    public void drawImage(){
        gd.setColor(Color.WHITE);
        gd.fillRect(0, 0, image.getWidth(), image.getHeight());//用白色填充画布
        for (int i = 0; i < 4; i++) {//循环四次画四个随机字符
            gd.setColor(getRandomColor());//设置画笔颜色，颜色为随机获取
            gd.setFont(new Font(getRandomFont(), Font.BOLD, image.getHeight()));//设置随机字体
            char rand = getRandomChar();text.append(rand);//获取一个随机字符，然后保存起来，以便之后获取
            gd.drawString(String.valueOf(rand), i*image.getHeight(), image.getHeight()-12);
            //绘制字符串 注：String.valueOf(char) 是将字符转换为字符串。
        }
        for (int i = 0; i < random.nextInt(2)+1; i++) {//随机画1~2跟干扰线
            gd.setColor(getRandomColor());
            gd.drawLine(0, random.nextInt(image.getHeight()), image.getWidth(), random.nextInt(image.getHeight()));
        }
    }
    // 图片保存到传入的路径
    public void output(String imagePath){
        try {
            ImageIO.write(image, "JPEG", new FileOutputStream(imagePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            this.drawImage();
            this.output(imagePath);
            e.printStackTrace();
        }
    }

    //获取到图片的Image（因为BufferedImage是Image的子类，所以直接返回）
    public Image getImage(){
        return image;
    }
    //获取图片的Icon的形式，主要用来往JLabel设置
    public Icon getIcon(){
        return new ImageIcon(this.getImage());
    }

    // 获取文件的二维数组
    public byte[] getBytes(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "JPEG", outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

    // 获取文本
    public StringBuffer getText() {
        return text;
    }
}
