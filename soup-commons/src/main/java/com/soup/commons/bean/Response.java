package com.soup.commons.bean;

public class Response {

    private Integer code;
    private String massage;
    private Object response;

    public Response() {
    }

    public Response(Integer code, String massage) {
        this.code = code;
        this.massage = massage;
    }

    public Response(Integer code, String massage, Object response) {
        this.code = code;
        this.massage = massage;
        this.response = response;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "SoupResponse{" +
                "code=" + code +
                ", massage='" + massage + '\'' +
                ", response=" + response +
                '}';
    }
}
