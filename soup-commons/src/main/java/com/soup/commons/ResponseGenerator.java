package com.soup.commons;

import com.soup.commons.bean.Response;

public interface ResponseGenerator {

    int success = 1;
    int fail = 2;
    int error = 3;

    /**
     * 响应成功 不带数据
     * @param massage
     * @return
     */
    Response success(String massage);

    /**
     * 响应成功 带数据
     * @param massage
     * @param response
     * @return
     */
    Response success(String massage,Object response);


    /**
     * 响应失败 不带数据
     * @param massage
     * @return
     */
    Response fail(String massage);

    /**
     * 响应失败 带数据
     * @param massage
     * @param response
     * @return
     */
    Response fail(String massage,Object response);

    /**
     * 系统错误 不带数据
     * @param massage
     * @return
     */
    Response error(String massage);

    /**
     * 系统错误 带数据
     * @param massage
     * @param response
     * @return
     */
    Response error(String massage,Object response);
}
