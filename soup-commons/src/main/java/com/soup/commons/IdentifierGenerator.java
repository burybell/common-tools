package com.soup.commons;

public interface IdentifierGenerator {


    /**
     * 获取 实体 对应的ID
     * @param entity
     * @return
     */
    Number nextId(Object entity);

    /**
     * 获取 实体 对应的UUID
     * @param entity
     * @return
     */
    String nextUUID(Object entity);

}
