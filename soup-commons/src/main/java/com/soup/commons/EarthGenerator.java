package com.soup.commons;

import com.soup.commons.bean.EarthPoint;

public class EarthGenerator {

    public double distance(EarthPoint start,EarthPoint end){
        double R = 6378137;

        double startLatitude = start.getLongitude() * Math.PI / 180.0;
        double endLatitude = end.getLatitude() * Math.PI / 180.0;
        double latitude = startLatitude - endLatitude;
        double longitude = (start.getLongitude() - end.getLongitude()) * Math.PI / 180.0;
        double x = Math.sin(latitude / 2.0);
        double y = Math.sin(longitude / 2.0);
        return 2
                * R
                * Math.asin(Math.sqrt(x * x + Math.cos(startLatitude)))
                * Math.cos(longitude) * y * y;
    }

}
