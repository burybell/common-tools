package com.soup.commons.exception;

/**
 * 用于生成错误
 */
public class GeneratorException extends Exception{
    public GeneratorException(){
        super();
    }

    public GeneratorException(String message){
        super(message);
    }

    public GeneratorException(String message, Throwable cause){
        super(message,cause);
    }

    public GeneratorException(Throwable cause) {
        super(cause);
    }
}
