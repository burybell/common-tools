package com.soup.commons;

import com.soup.commons.exception.GeneratorException;

import java.awt.*;

public class ColorTest {
    public static void main(String[] args) {
        ColorGenerator colorGenerator = new ColorGenerator();

        try {
            System.out.println(colorGenerator.generator("#ccc"));
        } catch (GeneratorException e) {
            e.printStackTrace();
        }

        System.out.println(colorGenerator.generator(Color.LIGHT_GRAY));
    }
}
